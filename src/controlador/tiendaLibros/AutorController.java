/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tiendaLibros;

import controlador.DAO.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.utiles.Utilidades;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import modelo.tiendaLibros.Autor;
import modelo.tiendaLibros.Libro;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author sebastian
 */
public class AutorController extends AdaptadorDao<Autor>{

    private Autor autor;
    private ListaEnlazadaServices<Autor> listaAutores;

    public AutorController() {
        super(Autor.class);
        listado();
    }
    
   

    public ListaEnlazadaServices<Autor> getListaAutores() {
        
        return listaAutores;
    }

    public void setListaAutores(ListaEnlazadaServices<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }

    public Autor getAutor() {
        if (this.autor == null) {
            this.autor = new Autor();
        }
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Boolean guardar() {
        try {            
            getAutor().setId(listaAutores.getSize()+1);
            guardar(getAutor());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar autor"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getAutor(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar autor"+e);
        }
        return false;
    }
    
    public ListaEnlazadaServices<Autor> listado() {
        setListaAutores(listar());
        return listaAutores;
    }

    
}
