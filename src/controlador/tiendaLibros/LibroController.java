/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tiendaLibros;

import controlador.DAO.AdaptadorDao;
import controlador.tda.lista.ListaEnlazadaServices;
import modelo.tiendaLibros.Libro;

/**
 *
 * @author sebastian
 */
public class LibroController extends AdaptadorDao<Libro>{
    private Libro libro;
    private ListaEnlazadaServices<Libro> listaLibros = new ListaEnlazadaServices<Libro>();

    public LibroController() {
        super(Libro.class);
        listado();
    }

    public Libro getLibro() {
        if(libro == null)
            libro = new Libro();
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public ListaEnlazadaServices<Libro> getListaLibros() {
        return listaLibros;
    }

    public void setListaLibros(ListaEnlazadaServices<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }
    
    public Boolean guardar() {
        try {            
            getLibro().setId(listaLibros.getSize()+1);
            guardar(getLibro());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar Libro"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getLibro(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar autor"+e);
        }
        return false;
    }
    
    public ListaEnlazadaServices<Libro> listado() {
        setListaLibros(listar());
        return listaLibros;
    }
    
}
