/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tiendaLibros.servicios;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tiendaLibros.AutorController;
import modelo.tiendaLibros.Autor;

/**
 *
 * @author sebastian
 */
public class AutorServicio {
    private AutorController ac = new AutorController();
    public Autor getAutor() {
        return ac.getAutor();
    }
    
    public ListaEnlazadaServices<Autor> getLista() {
        return ac.getListaAutores();
    }
    
    public ListaEnlazadaServices<Autor> getListaArchivo() {
        return ac.listado();
    }
    
    public Boolean guardar() {
       return ac.guardar();
    }
    
    public Boolean modificar(Integer pos) {
        return ac.modificar(pos);
    }
    
    public void setAutor(Autor autor) {
        ac.setAutor(autor);
    }
    
}
