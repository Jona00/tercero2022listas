/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tiendaLibros.servicios;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tiendaLibros.LibroController;
import modelo.tiendaLibros.Libro;

/**
 *
 * @author sebastian
 */
public class LibroServicio {
    private LibroController ac = new LibroController();
    public Libro getLibro() {
        return ac.getLibro();
    }
    
    public ListaEnlazadaServices<Libro> getLista() {
        return ac.getListaLibros();
    }
    
    public ListaEnlazadaServices<Libro> getListaArchivo() {
        return ac.listado();
    }
    
    public Boolean guardar() {
       return ac.guardar();
    }
    
    public Boolean modificar(Integer pos) {
        return ac.modificar(pos);
    }
    
    public void setLibro(Libro libro) {
        ac.setLibro(libro);
    }
}
