/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tiendaLibros;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@XmlRootElement
public class Libro extends Documento {
    
    private String issn;
    private Boolean revPares;

    

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public Boolean getRevPares() {
        return revPares;
    }

    public void setRevPares(Boolean revPares) {
        this.revPares = revPares;
    }

    @Override
    public String toString() {
         return getAutor() + " " + getTitulo() + " " + getTipoDocumento().toString() +" " +getRevPares();
    
    }
    
}
