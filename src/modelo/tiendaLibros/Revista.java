/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tiendaLibros;

import modelo.tiendaLibros.enums.Periocidad;

/**
 *
 * @author sebastian
 */
public class Revista extends Documento {
    
    private Periocidad periocidad;
    private String issn;

    

    public Periocidad getPeriocidad() {
        return periocidad;
    }

    public void setPeriocidad(Periocidad periocidad) {
        this.periocidad = periocidad;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }
    
}
