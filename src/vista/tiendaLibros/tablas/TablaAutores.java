/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tiendaLibros.tablas;

import controlador.tda.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;
import modelo.tiendaLibros.Autor;

/**
 *
 * @author sebastian
 */
public class TablaAutores extends AbstractTableModel {
    private ListaEnlazadaServices<Autor> lista;

    public ListaEnlazadaServices<Autor> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Autor> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Nro";
            case 1: return "Apellidos";
            case 2: return "Nombres";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        Autor autor = lista.obtenerDato(arg0);
        switch(arg1) {
            case 0: return (arg0+1);
            case 1: return autor.getApellidos();
            case 2: return autor.getNombres();
            default: return null;
        }
    }
    
    
    
    
}
