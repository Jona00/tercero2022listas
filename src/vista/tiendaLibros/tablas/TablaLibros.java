/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tiendaLibros.tablas;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tiendaLibros.servicios.AutorServicio;
import javax.swing.table.AbstractTableModel;
import modelo.tiendaLibros.Autor;
import modelo.tiendaLibros.Libro;

/**
 *
 * @author sebastian
 */
public class TablaLibros extends AbstractTableModel {
    private ListaEnlazadaServices<Libro> lista;

    public ListaEnlazadaServices<Libro> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Libro> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Nro";
            case 1: return "Autor";
            case 2: return "Titulo";
            case 3: return "ISSN";
            case 4: return "Edision";
            case 5: return "Editorial";
            case 6: return "Año";
            case 7: return "Nro Inv";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        Libro libro = lista.obtenerDato(arg0);
        switch(arg1) {
            case 0: return (arg0+1);
            case 1: return new AutorServicio().getListaArchivo().obtenerDato(libro.getAutor()-1);
            case 2: return libro.getTitulo();
            case 3: return libro.getIssn();
            case 4: return libro.getEdicion();
            case 5: return libro.getEditorial();
            case 6: return libro.getAnio();
            case 7: return libro.getInv();
            default: return null;
        }
    }
    
    
    
    
}
